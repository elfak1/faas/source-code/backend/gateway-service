package com.diplomski.gateway.util;

import lombok.experimental.UtilityClass;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@UtilityClass
public class ResponseUtil {

    public static Mono<Void> toErrorResponse(ServerWebExchange exchange, String err, HttpStatus httpStatus) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(httpStatus);
        return response.setComplete();
    }

    public static String getAuthHeader(ServerHttpRequest request, String headerName) {
        return request.getHeaders()
                .getOrEmpty(headerName)
                .get(0);
    }

    public static boolean isHeaderMissing(ServerHttpRequest request, String authHeaderName) {
        return !request.getHeaders().containsKey(authHeaderName);
    }
}
