package com.diplomski.gateway.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;

import java.util.Date;
import java.util.function.Function;

import static com.diplomski.gateway.configuration.Constants.SUBJECT_ID_CLAIM_NAME;

public class JWTUtil {

    private static final String secret = "Vz783rK8PVuqQ6WuSGjMHaUh9iC39UNhQXfbwu4HVTfBY6gNE3SdRdqUNB4i7stJVWwWwHG5A4Sx3fyWUjGbuidZnrnzJvXpuLCG";
    private static final JwtParser jwtParser = Jwts.parserBuilder().setSigningKey(secret).build();

    public static String getEmailFromToken(final String token) {
        return getClaimFromToken(Claims::getSubject, token);
    }

    public static Boolean isExpired(final String token) {
        final Date expiration = getClaimFromToken(Claims::getExpiration, token);
        return expiration.before(new Date());
    }

    public static String getId(final String token) {
        return getClaimFromToken(claims -> claims.get(SUBJECT_ID_CLAIM_NAME, String.class), token);
    }

    public static boolean isInvalid(String token) {
        try {
            return isExpired(token);
        } catch (Exception e) {
            return true;
        }
    }

    private static <T> T getClaimFromToken(Function<Claims, T> claimsResolver, final String token) {
        final Claims claims = jwtParser
                .parseClaimsJws(token)
                .getBody();

        return claimsResolver.apply(claims);
    }
}
