package com.diplomski.gateway.configuration;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
    public static final String SUBJECT_ID_CLAIM_NAME = "sub-id";
    public static final String SUBJECT_ID_REQUEST_HEADER_NAME = "x-jwt-sub-id";
}
