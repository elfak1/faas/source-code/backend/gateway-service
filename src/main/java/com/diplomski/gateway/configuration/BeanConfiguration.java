package com.diplomski.gateway.configuration;

import com.diplomski.gateway.configuration.filter.AuthenticationFilter;
import com.diplomski.gateway.configuration.filter.SecretApiKeyFilter;
import com.diplomski.gateway.routeprovider.RoutProvider;
import com.diplomski.gateway.routeprovider.config.RoutProviderConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public RoutProvider profileServiceRoutProvider(
            @Value("${profile.url}") final String baseUrl,
            @Value("${profile.port}") final String port,
            @Value("${profile.service-name}") final String profileServiceName,
            final SecretApiKeyFilter secretApiKeyFilter,
            final AuthenticationFilter authenticationFilter){
        return new RoutProvider(
                new RoutProviderConfig(baseUrl, port, profileServiceName),
                authenticationFilter,
                secretApiKeyFilter);
    }

    @Bean
    public RoutProvider projectServiceRoutProvider(
            @Value("${project.url}") final String baseUrl,
            @Value("${project.port}") final String port,
            @Value("${project.service-name}") final String projectServiceName,
            final SecretApiKeyFilter secretApiKeyFilter,
            final AuthenticationFilter authenticationFilter){
        return new RoutProvider(
                new RoutProviderConfig(baseUrl, port, projectServiceName),
                authenticationFilter,
                secretApiKeyFilter);
    }
}
