package com.diplomski.gateway.configuration.filter;

import com.diplomski.gateway.util.JWTUtil;
import com.diplomski.gateway.util.ResponseUtil;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import static com.diplomski.gateway.configuration.Constants.SUBJECT_ID_REQUEST_HEADER_NAME;

@Component
public class AuthenticationFilter implements GatewayFilter {

    public static final String JWT_TOKEN_PREFIX = "Bearer ";
    public static final String AUTH_HEADER_NAME = "Authorization";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();

        if (this.isAuthHeaderMissing(request))
            return ResponseUtil.toErrorResponse(exchange, "Authorization header is missing in request", HttpStatus.UNAUTHORIZED);

        final String token = this.getAuthHeader(request);

        if (JWTUtil.isInvalid(token))
            return ResponseUtil.toErrorResponse(exchange, "Authorization header is invalid", HttpStatus.UNAUTHORIZED);

        final String id = JWTUtil.getId(token);

        ServerHttpRequest mutatedRequest = exchange.getRequest()
                .mutate()
                .header(SUBJECT_ID_REQUEST_HEADER_NAME,id)
                .build();

        return chain.filter(exchange.mutate().request(mutatedRequest).build());
    }

    private String getAuthHeader(ServerHttpRequest request) {
        return  ResponseUtil.getAuthHeader(request, AUTH_HEADER_NAME)
                .replace(JWT_TOKEN_PREFIX, "");
    }

    private boolean isAuthHeaderMissing(ServerHttpRequest request) {
        return !request.getHeaders().containsKey(AUTH_HEADER_NAME);
    }
}
