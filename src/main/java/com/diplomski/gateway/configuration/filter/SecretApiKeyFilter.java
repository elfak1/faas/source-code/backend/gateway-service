package com.diplomski.gateway.configuration.filter;

import com.diplomski.gateway.util.ResponseUtil;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
public class SecretApiKeyFilter implements GatewayFilter {

    public static final String AUTH_HEADER_NAME = "x-secret-api-key";
    public static final String SECRET_API_KEY = "GX42bVUgHpYQ8RFANdGGNh6W";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();

        if (ResponseUtil.isHeaderMissing(request, AUTH_HEADER_NAME)) {
            return ResponseUtil.toErrorResponse(exchange, HttpStatus.UNAUTHORIZED.getReasonPhrase(), HttpStatus.UNAUTHORIZED);
        }

        final String secretApiKey = ResponseUtil.getAuthHeader(request, AUTH_HEADER_NAME);

        if (!secretApiKey.equals(SECRET_API_KEY))
            return ResponseUtil.toErrorResponse(exchange, HttpStatus.UNAUTHORIZED.getReasonPhrase(), HttpStatus.UNAUTHORIZED);

        return chain.filter(exchange);
    }
}

