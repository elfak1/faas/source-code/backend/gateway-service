package com.diplomski.gateway.configuration;

import com.diplomski.gateway.routeprovider.RoutProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@RequiredArgsConstructor
public class GatewayConfiguration {

    private final List<RoutProvider> routProviders;

    @Bean
    public RouteLocator myRoutes(RouteLocatorBuilder routeLocatorBuilder) {
        final RouteLocatorBuilder.Builder builder = routeLocatorBuilder.routes();

        routProviders.forEach(routProvider -> builder
                .route(routProvider::getPrivateRout)
                .route(routProvider::getPublicRout)
                .route(routProvider::getAuthenticatedRout));

        return builder.build();
    }
}
