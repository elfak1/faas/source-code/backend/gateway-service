package com.diplomski.gateway.routeprovider;

import com.diplomski.gateway.configuration.filter.AuthenticationFilter;
import com.diplomski.gateway.configuration.filter.SecretApiKeyFilter;
import com.diplomski.gateway.routeprovider.config.RoutProviderConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.builder.Buildable;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;

@RequiredArgsConstructor
public class RoutProvider {

    private final RoutProviderConfig routProviderConfig;
    private final AuthenticationFilter authenticationFilter;
    private final SecretApiKeyFilter secretApiKeyFilter;

    public Buildable<Route> getAuthenticatedRout(PredicateSpec predicateSpec) {
        return predicateSpec
                .path(routProviderConfig.getAuthenticatedPath())
                .filters(f -> f.filter(authenticationFilter))
                .uri(routProviderConfig.getUrl());
    }

    public Buildable<Route> getPrivateRout(PredicateSpec predicateSpec) {
        return predicateSpec
                .path(routProviderConfig.getPrivatePath())
                .filters(f -> f.filter(secretApiKeyFilter))
                .uri(routProviderConfig.getUrl());
    }

    public Buildable<Route> getPublicRout(PredicateSpec predicateSpec) {
        return predicateSpec
                .path(routProviderConfig.getPublicPath())
                .uri(routProviderConfig.getUrl());
    }
}
