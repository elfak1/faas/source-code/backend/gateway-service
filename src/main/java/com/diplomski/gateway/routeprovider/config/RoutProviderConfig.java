package com.diplomski.gateway.routeprovider.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class RoutProviderConfig {

    private final String baseUrl;

    private final String port;

    private final String serviceName;

    public String getUrl() {
        return baseUrl + ":" + port;
    }

    public String getAuthenticatedPath() {
        return "/api/" + serviceName + "/**";
    }

    public String getPublicPath() {
        return "/api/" + serviceName + "/public/**";
    }


    public String getPrivatePath() {
        return "/api/" + serviceName + "/private/**";
    }
}
